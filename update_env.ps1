#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory = $false, Position = 1)]
    [string] $ResourcePath,

    [Parameter(Mandatory = $false, Position = 2)]
    [string[]] $Components
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq "")) {
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

# Switch kubectl context to minikube
Switch-EnvKubeContext

if ($null -eq $Components) {
    $Components = @(
        "kubernetes",
        "namespace",
        "environment"
    )
}

try {
    $totalStartTime = $(Get-Date)
    $totalStatus = "SUCCESS"
    $statuses = @()

    # Clear errors variable for clean logging
    $error.clear()

    # Create components in the order they defined
    foreach ($component in $Components) {    
        try {
            $startTime = $(Get-Date)
            $status = "SUCCESS"

            Write-EnvInfo -Component $component "Started updating $component component." -Delimiter

            . "$rootPath/$component/update.ps1" -ConfigPath "$ConfigPath" -ResourcePath "$ResourcePath"

            Write-EnvInfo -Component $component "Completed updating $component component." -Delimiter
        } 
        # Catch and log any errors
        catch {
            $status = "FAIL"
            $totalStatus = "FAIL"

            $error
            Write-EnvError -Component $component "Can't update the $component component. See logs above."
        }
        finally {
            $elapsedTime = $(Get-Date) - $startTime
            $duration = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
            $statuses += @{ Component = $component; Duration = $duration; Status = $status }

            if ($status -eq "FAIL") {
                Write-EnvError -Component $Component "$component update $status in $duration (HH:mm:ss.ms)"
            }
            else {
                Write-EnvInfo -Component $Component "$component update $status in $duration (HH:mm:ss.ms)" -Color "Yellow"
            }
        }
    }
}
finally {
    $totalElapsedTime = $(Get-Date) - $totalStartTime
    $totalDuration = "{0:HH:mm:ss.ffff}" -f ([datetime]$totalElapsedTime.Ticks)

    Write-EnvInfo -Component "environment" "Overall update status: $totalStatus" -Color "Yellow"

    $statuses | ForEach-Object { [PSCustomObject]$_ } | Format-Table -AutoSize

    Write-EnvInfo -Component "environment" "Total update duration: $totalDuration (HH:mm:ss.ms)" -Color "Yellow"
}
