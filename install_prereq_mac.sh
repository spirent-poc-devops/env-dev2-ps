#!/bin/bash
while getopts c: option
do
case "${option}"
in
c) CONFIG_FILE=${OPTARG};;
esac
done
[ -z "$CONFIG_FILE" ] && CONFIG_FILE="config/default_config.json"
echo "Config file being used:"$CONFIG_FILE". To use any other config file pass the path using -c arg"

brew update

# Install powershell
brew install --cask powershell

# Install docker and kubernetes
brew install kubernetes-cli
brew install --cask docker
brew install minikube
brew install helm