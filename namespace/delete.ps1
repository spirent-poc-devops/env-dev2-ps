#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$templateParams = @{
    namespace        = $namespace;
}

# Set variables from config
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/namespace.yml" -OutputPath "$PSScriptRoot/../temp/namespace.yml" -Params1 $templateParams

# Delete k8s namespace
kubectl delete -f "$PSScriptRoot/../temp/namespace.yml"

# Delete results and save resource file to disk
Remove-EnvMapValue -Map $resources -Key "k8s.namespace"
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
